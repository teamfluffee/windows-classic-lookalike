#!/bin/bash

classic_path=$(dirname -- "$(readlink -f -- "$BASH_SOURCE")")

function strip_directory () {
  path=$1
  if [[ $# -gt 1 ]] ; then
    for (( i=0; i < $2; i++ )) do
      path=$(sed 's,/*[^/]\+/*$,,' <<< $path)
    done
  else
    path=$(sed 's,/*[^/]\+/*$,,' <<< $path)
  fi
  echo $path
}

function move_icons() {
  path=$(sed s'/\/$//' <<< "$1")
  mkdir -p $path
  for d in ${classic_path}/theme-files/icons/*/ ; do
    cp -r $d $path/
  done
}

function download_tahoma() {
  path=$1
  if [[ "$path" != */ ]] ; then
    path=$path/
  fi
  mkdir -p $path
  wget https://source.winehq.org/source/fonts/tahoma.ttf?_raw=1 -O ${path}tahoma.ttf
  wget wget https://source.winehq.org/source/fonts/tahomabd.ttf?_raw=1 -O ${path}tahomabd.ttf
}

function move_icewm_theme() {
  mkdir -p /usr/share/icewm/themes/
  cp -R ${classic_path}/theme-files/icons/WinClassic2 /usr/share/icewm/themes/
}

function copy_hidden_file() {
  if [[ $2 == */ ]] ; then
    file_name=$(basename $2)
  else
    file_name=$(sed "s/.*\///" <<< $2)
  fi
  cp -R ${2} /home/${1}/.${file_name}
}

function setup_idesk_icons() {
  sed -i "s/username/${1}/g" ${classic_path}/theme-files/idesktop/*
  mv ${classic_path}/theme-files/idesktop /home/$1/.idesktop
}

function modify_xstartup() {
  sed -i 's/twm \&/idesk \&/g' /home/$1/.vnc/xstartup
  echo 'icewm-session &' >> /home/$1/.vnc/xstartup
}

function setup_gtk3() {
  mkdir -p /home/$1/.config/gtk-3.0/
  cp ${classic_path}/theme-files/gtkrc-2.0 /home/$1/.config/gtk-3.0/settings.ini
}
