#!/bin/bash

account_name=$1
classic_path=$(dirname -- "$(readlink -f -- "$BASH_SOURCE")")

function install_dependencies() {
  apt-get -y update
  apt-get install -y leafpad galculator xfe icewm idesk rxvt-unicode gtk2-engines
}

function strip_directory () {
  path=$1
  if [[ $# -gt 1 ]] ; then
    for (( i=0; i < $2; i++ )) do
      path=$(sed 's,/*[^/]\+/*$,,' <<< $path)
      if [[ $(basename $path) -eq "windows-classic-lookalike" ]] ; then
        break
     fi
    done
  else
    path=$(sed 's,/*[^/]\+/*$,,' <<< $path)
  fi
  echo $path
}

if [[ $# -eq q ]] ; then
  echo "Error, you did not only specify a name as a parameter, ending"
  exit 1
fi

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

classic_path=$(strip_directory $classic_path 2)
install_dependencies
source ${classic_path}/shared-utilities.sh
move_icewm_theme
move_icons /home/${account_name}/.icons
setup_idesk_icons ${account_name}
download_tahoma /home/${account_name}/.fonts
copy_hidden_file ${account_name} ${classic_path}/theme-files/icewm/
setup_gtk3 ${account_name}

for filename in ${classic_path}/theme-files/* ; do
  if [[ -f $filename  ]] ; then
    copy_hidden_file ${account_name} $filename
  fi
done

modify_xstartup ${account_name}
chown -R ${account_name} /home/${account_name}/*
chown -R ${account_name} /home/${account_name}/.*
